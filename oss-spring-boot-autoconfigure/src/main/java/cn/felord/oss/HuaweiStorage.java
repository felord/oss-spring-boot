package cn.felord.oss;

import cn.felord.autoconfigure.OSSProperties;
import com.obs.services.ObsClient;
import com.obs.services.model.ObjectMetadata;
import com.obs.services.model.ObsObject;

import java.io.InputStream;

/**
 * @author felord.cn
 * @since 1.0.8.RELEASE
 */
public class HuaweiStorage implements Storage {
    private final ObsClient obsClient;
    private final String endpoint;

    public HuaweiStorage(OSSProperties.Huawei huawei) {
        this.endpoint = huawei.getEndpoint();
        this.obsClient = new ObsClient(huawei.getAccessKeyId(),
                huawei.getAccessKeySecret(),
                this.endpoint);
    }


    @Override
    public void putObject(String bucketName, String objectName, InputStream inputStream, String contentType) throws Exception {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(contentType);
        this.obsClient.putObject(bucketName, objectName, inputStream, metadata);
    }

    @Override
    public InputStream getObject(String bucketName, String objectName) throws Exception {
        ObsObject object = this.obsClient.getObject(bucketName, objectName);
        return object.getObjectContent();
    }

    @Override
    public String getObjectUrl(String bucketName, String objectName) throws Exception {
        return "https://" + bucketName + "." + this.endpoint + "/" + objectName;
    }

    @Override
    public void removeObject(String bucketName, String objectName) throws Exception {
        this.obsClient.deleteObject(bucketName, objectName);
    }
}
