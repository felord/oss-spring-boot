package cn.felord.autoconfigure;

import cn.felord.oss.HuaweiStorage;
import cn.felord.oss.Storage;
import com.obs.services.ObsClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Huawei oss configuration.
 *
 * @author felord.cn
 * @since 1.0.8.RELEASE
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnProperty(prefix = "oss.huawei", name = "active", havingValue = "true")
@ConditionalOnClass(ObsClient.class)
class HuaweiOSSConfiguration {

    /**
     * Huawei storage storage.
     *
     * @param ossProperties the oss properties
     * @return the storage
     */
    @Bean
    @Qualifier("huaweiStorage")
    Storage huaweiStorage(OSSProperties ossProperties) {
        OSSProperties.Huawei huawei = ossProperties.getHuawei();
        return new HuaweiStorage(huawei);
    }
}
